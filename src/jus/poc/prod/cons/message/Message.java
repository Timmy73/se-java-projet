package jus.poc.prod.cons.message;

public class Message {
	
	/**
	 * Variable static utilisable uniquement dans la classe Message.
	 * Sert à identifier de façon unique un message
	 */
	private static int idSeq = 0;
	
	/**
	 * id du message courant associé au msg.
	 */
	private int id;
	private String msg;
	
	/**
	 * Constructeur Message avec 1 paramètre de type String.
	 * @param msg String Contenu du message
	 */
	public Message(String msg) {
		this(Message.getNextId(), msg);
	}
	
	/**
	 * Constructeur Message avec choix id et msg donnés
	 * @param id int  Id du nouveau message
	 * @param msg String Contenu message
	 */
	public Message(int id, String msg) {
		this.id		= id;
		this.msg	= msg;
	}
	
	/**
	 * Permet d'obtenir un id
	 * @return int 
	 */
	public static int getNextId() {
		Message.idSeq++;
		return Message.idSeq;
	}
	
	// Getters && Setters
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String toString() {
		return "["+this.id+"] "+this.msg;
	}
	
}
