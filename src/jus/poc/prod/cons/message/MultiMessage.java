package jus.poc.prod.cons.message;

public class MultiMessage extends Message {
	
	/**
	 * int représentant le nombre d'occurence de message
	 */
	private int n;

	/**
	 * Constructeur MultiMessage utilise le constructeur message (super).
	 * Permet de définir le nombre d'occurence du message
	 * @param id int
	 * @param msg String 
	 * @param n int 
	 */
	public MultiMessage(int id, String msg, int n) {
		super(id,msg);
		this.n = n;
	}
	
	/**
	 *  Constructeur attribuant un Id à un message donné en n occurence
	 * @param msg String
	 * @param n int
	 */
	public MultiMessage(String msg, int n) {
		this(Message.getNextId(), msg, n);
	}
	
	/**
	 * Constructeur attribuant un Id à un message donné.
	 * défini le nombre d'occurence à 1
	 * @param msg String
	 */
	public MultiMessage(String msg) {
		this(Message.getNextId(), msg, 1);
	}
	
	// Getter & Setter
	
	public int getN() {
		return this.n;
	}
	
	public void setN(int n) {
		this.n = n;
	}

}
