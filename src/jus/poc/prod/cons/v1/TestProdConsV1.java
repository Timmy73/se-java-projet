package jus.poc.prod.cons.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import jus.poc.prod.cons.tests.TestProdCons;

public class TestProdConsV1 extends TestProdCons {

	/**
	 * ProdConsBuffer: buffer où les producteurs stockent les messages produits
	 * et où les consommateurs consomment
	 * Producer : tableau où sont stockés les producteurs
	 * Consumer : tableau où sont stockés les consommateurs
	 */
	private ProdConsBuffer	buffer;
	private Producer		producers[];
	private Consumer		consumers[];

	/**
	 * Constructeur pouvant lever des exceptions si fichier absent ou format eronné
	 * Création tableau buffer, producers, consumers en fonction des tailles données 
	 * @throws InvalidPropertiesFormatException
	 * @throws IOException
	 */
	public TestProdConsV1() throws InvalidPropertiesFormatException, IOException {
		super(TestProdConsV1.class.getResourceAsStream("config.xml"));

		this.buffer		= new ProdConsBuffer(this.bufferSize);
		this.producers	= new Producer[this.nProd];
		this.consumers	= new Consumer[this.nCons];

	}

	/**
	 * Initialisation des threads consommateurs et producteurs 
	 */
	private void init() {
		this.threads.clear(); //Méthode permettant d'initialiser en vidant les listes des threads consommateurs et producteurs
		for (int i = 0, j = 0; i < this.nProd || j < this.nCons; i++, j++) { // création des producteurs et consommateurs
			if (i < this.nProd) {
				this.producers[i] = new Producer(this.minProd, this.maxProd, this.prodTime, this.buffer);
				this.threads.add(this.producers[i]);
			}
			if (j < this.nCons) {
				this.consumers[j] = new Consumer(this.consTime, this.buffer);
				this.threads.add(this.consumers[j]);
			}
		}
		Collections.shuffle(threads); // mélanger consommateurs, producteur 
	}

	public void test() throws Exception {
		this.init();

		long start = System.currentTimeMillis();

		for (int k = 0; k < this.threads.size(); k++) {
			this.threads.get(k).start();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else
			throw new Exception("Some messsages are still in buffer");

		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");

	}

	public void testBufferSize() throws Exception {
		int bz = this.bufferSize + (int) (Math.random() * (this.bufferMaxSize - this.bufferSize) + 1);
		
		this.buffer = new ProdConsBuffer(bz);
		this.init();
		
		long start = System.currentTimeMillis();
		
		for (int k = 0; k < this.threads.size(); k++) {
			this.threads.get(k).start();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else
			throw new Exception("Some messsages are still in buffer");
		
		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

}
