package jus.poc.prod.cons.v1;

import jus.poc.prod.cons.message.Message;

public class ProdConsBuffer implements IProdConsBuffer {

	
	/**
	 * nbCurrMsg : nombre message courant
	 * nbAddedMsg : nombre message ajouté depuis la création du buffer
	 * bufferSize : taille du buffer
	 * messages : tableau contenant les messages
	 */
	private int	nbCurrMsg;
	private int	nbAddedMsg;
	private int	bufferSize;

	private Message messages[];

	public ProdConsBuffer(int bufferSize) {
		this.bufferSize	= bufferSize;
		this.nbAddedMsg	= 0;
		this.nbCurrMsg	= 0;

		this.messages	= new Message[this.bufferSize];
	}

	@Override
	synchronized public void put(Message m) throws InterruptedException {

		while (this.nbCurrMsg >= this.messages.length) {
			wait();
		}

		this.messages[this.nbCurrMsg] = m;

		this.nbAddedMsg++;
		this.nbCurrMsg++;
		notifyAll();
	}

	@Override
	synchronized public Message get() throws InterruptedException {
		while (this.nbCurrMsg <= 0) {
			wait();
		}

		Message m = this.messages[0];
		for (int i = 1; i < this.nbCurrMsg; i++) {
			this.messages[i - 1] = this.messages[i];
		}

		this.nbCurrMsg--;
		this.messages[this.nbCurrMsg] = null;

		notifyAll();
		return m;
	}

	@Override
	public int nmsg() {
		return this.nbCurrMsg;
	}

	@Override
	public int totmsg() {
		return this.nbAddedMsg;
	}

}
