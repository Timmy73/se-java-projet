package jus.poc.prod.cons.v1;

import jus.poc.prod.cons.message.Message;

public class Producer extends Thread {

	
	/**
	 * prodTime : temps d'une production
	 * nbToProduce : nombre message à produire
	 * min : nombre minimum de message à produire
	 * max : nombre maximum de message à produire
	 */
	private int	prodTime;
	private int	nbToProduce;
	private int	min;
	private int	max;

	private ProdConsBuffer buff;

	/** Génération du nombre de message à produire entre les valeurs min et max
	 * 
	 * @param min int 
	 * @param max int
	 * @param prodTime int
	 * @param buff ProdConsBuffer
	 */
	public Producer(int min, int max, int prodTime, ProdConsBuffer buff) {

		this.min		= min;
		this.max		= max;
		this.prodTime	= prodTime;
		this.buff		= buff;

		this.nbToProduce = this.min + (int) (Math.random() * ((this.max - this.min) + 1));

	}

	@Override
	synchronized public void run() {
		try {
			for (int i = 0; i < this.nbToProduce; i++) {
				Thread.sleep(this.prodTime);
				this.buff.put(new Message("Message " + i + " from producer n° " + this.getId()));

				if (TestProdConsV1.DEBUG)
					System.out.println("[PRODUCER] Message generate");
			}
			System.out.println("[PRODUCER] Producer " + this.getId() + " finished");
			notify();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
