package jus.poc.prod.cons.v1;

public class Consumer extends Thread {
	/**
	 * consTime : temps d'une consommation
	 * buffer : buffer à utiliser
	 */
	private int				consTime;
	private ProdConsBuffer	buffer;

	/**
	 * Constructeur permettant de définir le temps d'une conso et le buffer utilisé
	 * @param consTime int
	 * @param buffer ProdConsBuffer
	 */
	public Consumer(int consTime, ProdConsBuffer buffer) {
		this.consTime	= consTime;
		this.buffer		= buffer;
	}

	/**
	 * Lancement du Consumer
	 */
	@Override
	public void run() {
		try {
			while (true) {
				if (TestProdConsV1.DEBUG)
					System.out.println("[CONSUMER] : " + this.buffer.get());
				else
					this.buffer.get();
				
				sleep(this.consTime);
			}
		} catch (InterruptedException e) {
		}
	}
}
