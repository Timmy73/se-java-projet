package jus.poc.prod.cons.v2;

public class Consumer extends Thread {
	/**
	 * Temps d'une consomation
	 */
	private int				consTime;
	
	/*
	 * Buffer à utiliser
	 */
	private ProdConsBuffer	buffer;

	/**
	 * 
	 * @param consTime Temps de la consomation
	 * @param buffer
	 */
	public Consumer(int consTime, ProdConsBuffer buffer) {
		this.consTime	= consTime;
		this.buffer		= buffer;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if(TestProdConsV2.DEBUG)
					System.out.println("[CONSUMER] : " + this.buffer.get());
				else
					this.buffer.get();
				sleep(this.consTime);
			}
		} catch (InterruptedException e) {
		}
	}
}
