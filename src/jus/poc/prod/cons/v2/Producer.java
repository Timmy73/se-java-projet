package jus.poc.prod.cons.v2;

import jus.poc.prod.cons.message.Message;

public class Producer extends Thread {

	/**
	 * Temps d'une production
	 */
	private int		prodTime;
	
	/**
	 * Nombre de message à produire
	 */
	private int		nbToProduce;
	
	/**
	 * Nombre minimal de message à produire
	 */
	private int		min;
	
	/**
	 * Nombre maximal de message à produire
	 */
	private int		max;
	
	/**
	 * 
	 */
	private boolean	finished	= false;

	private ProdConsBuffer buff;

	public Producer(int min, int max, int prodTime, ProdConsBuffer buff) {

		this.min		= min;
		this.max		= max;
		this.prodTime	= prodTime;
		this.buff		= buff;

		this.nbToProduce = this.min + (int) (Math.random() * ((this.max - this.min) + 1));

	}

	@Override
	synchronized public void run() {
		try {
			for (int i = 0; i < this.nbToProduce; i++) {
				Thread.sleep(this.prodTime);
				this.buff.put(new Message("Message " + i + " from producer n° " + this.getId()));
				if(TestProdConsV2.DEBUG)
					System.out.println("[PRODUCER] Message generate");
			}
			this.finished = true;
			System.out.println("[PRODUCER] Producer "+this.getId()+" finished");
			notify();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isFinished() {
		return finished;
	}

}
