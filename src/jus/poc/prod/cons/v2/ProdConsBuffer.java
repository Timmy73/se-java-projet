package jus.poc.prod.cons.v2;

import java.util.concurrent.Semaphore;
import jus.poc.prod.cons.message.Message;

public class ProdConsBuffer implements IProdConsBuffer {

	/**
	 * Nombre de message actuellement dans le buffer 
	 */
	private int	nbCurrMsg;
	
	/**
	 * Nombre de messages ajoutés depuis la création du buffer
	 */
	private int	nbAddedMsg;
	
	/**
	 * Taille du buffer
	 */
	private int	bufferSize;

	/**
	 * Tableau contenant tous les messages en attente dans le buffer
	 */
	private Message		messages[];
	
	/**
	 * Semaphore appliqué sur les producteurs
	 */
	private Semaphore	semProd;
	
	/**
	 * Semaphore appliqué sur les consomateurs
	 */
	private Semaphore	semCons;

	/**
	 * Permet d'instancier un buffer pour une taille donnée et avec les deux sémaphore de taille 1
	 * @param bufferSize
	 */
	public ProdConsBuffer(int bufferSize) {
		this(bufferSize,1,1);
	}

	/**
	 * Permet d'instancier un buffer pour une taille donnée et avec deux tailles données pour les sémpahores
	 * @param bufferSize Taille du buffer
	 * @param semConsSz Taille sémaphore des consomateurs
	 * @param semProdSz Taille sémaphore des producteurs
	 */
	public ProdConsBuffer(int bufferSize, int semConsSz, int semProdSz) {
		this.bufferSize	= bufferSize;
		this.nbAddedMsg	= 0;
		this.nbCurrMsg	= 0;

		this.messages	= new Message[this.bufferSize];
		this.semProd	= new Semaphore(semProdSz);
		this.semCons	= new Semaphore(semConsSz);
	}

	@Override
	public void put(Message m) throws InterruptedException {

		this.semProd.acquire();
		synchronized (this) {
			while (this.nbCurrMsg >= this.messages.length) {
				wait();
			}
			this.messages[this.nbCurrMsg] = m;

			this.nbAddedMsg++;
			this.nbCurrMsg++;
			notifyAll();
		}
		this.semProd.release();

	}

	@Override
	public Message get() throws InterruptedException {
		Message m;

		this.semCons.acquire();

		synchronized (this) {
			while (this.nbCurrMsg <= 0) {
				wait();
			}

			m = this.messages[0];
			for (int i = 1; i < this.nbCurrMsg; i++) {
				this.messages[i - 1] = this.messages[i];
			}

			this.nbCurrMsg--;
			this.messages[this.nbCurrMsg] = null;

			notifyAll();
		}
		this.semCons.release();
		return m;
	}

	@Override
	public int nmsg() {
		return this.nbCurrMsg;
	}

	@Override
	public int totmsg() {
		return this.nbAddedMsg;
	}

}
