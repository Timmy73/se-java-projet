package jus.poc.prod.cons.v4;

import jus.poc.prod.cons.message.*;

public class ProdConsBuffer implements IProdConsBuffer {

	private int	nbCurrMsg;
	private int	nbAddedMsg;
	private int	bufferSize;

	private Message		messages[];

	public ProdConsBuffer(int bufferSize) {
		this.bufferSize = bufferSize;

		this.nbAddedMsg	= 0;
		this.nbCurrMsg	= 0;

		this.messages	= new Message[this.bufferSize];
	}

	@Override
	synchronized public void put(Message m) throws InterruptedException {
		while (this.nbCurrMsg >= this.messages.length)
			wait();

		this.messages[this.nbCurrMsg] = m;

		this.nbAddedMsg++;
		this.nbCurrMsg++;
		notifyAll();
	}

	@Override
	synchronized public Message get() throws InterruptedException {
		Message m;

		while (this.nbCurrMsg <= 0)
			wait();

		m = this.messages[0];
		if (m instanceof MultiMessage) { // Cas où le message est un multimessage
			MultiMessage mm = (MultiMessage) m;

			mm.setN(mm.getN() - 1); // On réduit le nombre d'exemplaire dans le buffer
			if (mm.getN() <= 0) { // Si plus aucun exemplaire, on retire le message et on notifie.
				this.removeMsg();
				notifyAll();
			}
		} else { // Cas message "classique", on fait avant.
			this.removeMsg();
			notifyAll();
		}

		return m;
	}

	@Override
	public Message[] get(int n) throws InterruptedException {
		Message[] returns = new Message[n];

		if (n <= 1) {
			returns[0] = this.get();
		} else {
			for (int i = 0; i < n; i++) {
				returns[i] = this.get();
			}
		}

		return returns;
	}

	@Override
	public int nmsg() {
		return this.nbCurrMsg;
	}

	@Override
	public int totmsg() {
		return this.nbAddedMsg;
	}

	private void removeMsg() {
		for (int i = 1; i < this.nbCurrMsg; i++) {
			this.messages[i - 1] = this.messages[i];
		}

		this.nbCurrMsg--;
		this.messages[this.nbCurrMsg] = null;
	}

}
