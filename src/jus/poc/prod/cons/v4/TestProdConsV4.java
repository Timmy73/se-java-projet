package jus.poc.prod.cons.v4;

import java.io.IOException;
import java.lang.Thread.State;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;

import jus.poc.prod.cons.tests.TestProdCons;

public class TestProdConsV4 extends TestProdCons {

	private int	nProdMaxMessage;
	private int	nProdMessage	= 2;

	private ProdConsBuffer	buffer;
	private Producer		producers[];
	private Consumer		consumers[];

	public TestProdConsV4() throws InvalidPropertiesFormatException, IOException {
		super(TestProdConsV4.class.getResourceAsStream("config.xml"));

		this.nProdMaxMessage = Integer.parseInt(conf.getProperty("nWMaxMessage"));

		this.buffer		= new ProdConsBuffer(bufferSize);
		this.producers	= new Producer[nProd];
		this.consumers	= new Consumer[nCons];
	}

	/**
	 * Initialisation des threads de consumers et producers.
	 */
	private void init() {
		this.threads.clear(); // On néttoie la liste des threads

		for (int i = 0, j = 0; i < this.nProd || j < this.nCons; i++, j++) { // On créer les différents threads
			if (i < this.nProd) {
				this.producers[i] = new Producer(this.minProd, this.maxProd, this.prodTime, this.buffer,
						this.nProdMessage);
				this.threads.add(this.producers[i]);
			}
			if (j < this.nCons) {
				this.consumers[j] = new Consumer(this.consTime, this.buffer);
				this.threads.add(this.consumers[j]);
			}
		}
		Collections.shuffle(this.threads); // Mélange de la liste des threads pour lancement aléatoire
	}

	@Override
	public void test() throws Exception {
		this.init();

		long start = System.currentTimeMillis();

		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}

		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	@Override
	public void testBufferSize() throws Exception {
		int bz = this.bufferSize + (int) (Math.random() * (this.bufferMaxSize - this.bufferSize) + 1);

		this.buffer = new ProdConsBuffer(bz);
		this.init();

		long start = System.currentTimeMillis();

		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}

		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	/**
	 * Ce test consiste à faire varier le nombre exemplaire de chaque message
	 * @throws Exception
	 */
	public void testNProd() throws Exception {
		this.nProdMessage	= 2 + (int) (Math.random() * (this.nProdMaxMessage - 2) + 1);
		this.buffer			= new ProdConsBuffer(1);
		this.init();

		long start = System.currentTimeMillis();

		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}

		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	/**
	 * Ce test consiste à faire varier le nombre exemplaire de chaque message et la taille du buffer
	 * @throws Exception
	 */
	public void testNProdBufSize() throws Exception {
		int bz = this.bufferSize + (int) (Math.random() * (this.bufferMaxSize - this.bufferSize) + 1);

		this.nProdMessage	= 2 + (int) (Math.random() * (this.nProdMaxMessage - 2) + 1);
		this.buffer			= new ProdConsBuffer(bz);
		this.init();

		long start = System.currentTimeMillis();

		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}

		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	/**
	 * Ce test doit échoué, car nous testons le cas où il y a moins de consomateurs
	 * que de nombre d'exemplaire d'un message.
	 * 
	 * @throws Exception
	 */
	public void testFail() throws Exception {

		// Pas d'évaluation de performance car le test doit lever un exception.

		int nProdBlocked = 0, nConsBlocked = 0;

		this.nCons			= 2;
		this.nProdMessage	= 3;
		this.buffer			= new ProdConsBuffer(1);
		this.init();

		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		// On test l'état des consomateurs
		for (int i = 0; i < this.nCons; i++) {
			if (this.consumers[i].getState() != State.RUNNABLE)
				nConsBlocked++;
		}

		// On test l'état des producteurs
		for (int i = 0; i < this.nProd; i++) {
			if (this.producers[i].getState() != State.RUNNABLE)
				nProdBlocked++;
		}

		if (nConsBlocked >= this.nCons || nProdBlocked >= this.nProd) { // Si tous sont bloqués, on lève une exception.
			throw new IllegalStateException();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}

	}
}
