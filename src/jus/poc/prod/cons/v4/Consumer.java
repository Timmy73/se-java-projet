package jus.poc.prod.cons.v4;

import jus.poc.prod.cons.message.*;
import jus.poc.prod.cons.tests.TestProdCons;

public class Consumer extends Thread {
	private int				consTime;
	private int				nCons;
	private ProdConsBuffer	buffer;

	public Consumer(int consTime, ProdConsBuffer buffer, int nCons) {
		this.consTime	= consTime;
		this.buffer		= buffer;
		this.nCons		= nCons;
	}
	
	public Consumer(int consTime, ProdConsBuffer buffer) {
		this(consTime, buffer, 1);
	}

	@Override
	public void run() {
		try {
			while (true) {
				// Comme on attend que tous les exemplaire d'un message soient consomés et que la notification associé à cette contrainte 
				//est dans le buffer, nous avons créé ce bloque synchronisé sur le buffer
				synchronized (this.buffer) {
					Message m = this.buffer.get();
					sleep(this.consTime);
					
					if(TestProdCons.DEBUG)
						System.out.println("[CONSUMER]["+this.getId()+"] : " + m);

					if (m instanceof MultiMessage) {
						while (((MultiMessage) m).getN() > 0)
							this.buffer.wait();
					}
				}
			}
		} catch (InterruptedException e) {
		}
	}
}
