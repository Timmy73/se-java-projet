package jus.poc.prod.cons.v4;

import jus.poc.prod.cons.message.MultiMessage;
import jus.poc.prod.cons.tests.TestProdCons;

public class Producer extends Thread {

	private int		prodTime;
	private int		nbToProduce;
	private int		min, max;
	private int		nExemplaire;
	private boolean	finished	= false;

	private ProdConsBuffer buff;

	public Producer(int min, int max, int prodTime, ProdConsBuffer buff, int nExemplaire) {

		this.min			= min;
		this.max			= max;
		this.prodTime		= prodTime;
		this.buff			= buff;
		this.nExemplaire	= nExemplaire;

		this.nbToProduce = this.min + (int) (Math.random() * ((this.max - this.min) + 1));

	}

	@Override
	public void run() {
		try {
			// Comme on attend que tous les exemplaire d'un message soient consomés et que la notification associé à cette contrainte 
			//est dans le buffer, nous avons créé ce bloque synchronisé sur le buffer
			synchronized (this.buff) {
				for (int i = 0; i < this.nbToProduce; i++) {
					MultiMessage m = new MultiMessage("Message " + i + " from producer n° " + this.getId(),
							this.nExemplaire);

					Thread.sleep(this.prodTime);
					this.buff.put(m);
					
					if(TestProdCons.DEBUG)
						System.out.println("[PRODUCER] Message generate");

					while (m.getN() > 0) {
						this.buff.wait();
					}
				}
			}

			this.finished = true;
			System.out.println("[PRODUCER] Producer " + this.getId() + " finished");
			// Ce bloque synchronisé existe, car nous faisons un join dans les tests.
			synchronized (this) {
				this.notifyAll();
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isFinished() {
		return finished;
	}

}
