package jus.poc.prod.cons.v3;

import jus.poc.prod.cons.message.Message;

public interface IProdConsBuffer {
	
	/**
	 * Put the message m in the prodcons buffer
	 * @param m Message to add
	 * @throws InterruptedException
	 */
	public void put(Message m) throws InterruptedException;
	
	/**
	 * Retrieve a message from the prodcons buffer, following a fifo order
	 * @return Message
	 * @throws InterruptedException
	 */
	public Message get() throws InterruptedException;
	
	/**
	 * Retrieve n messages from the prodcons buffer, following a fifo order
	 * @return Message[]
	 * @throws InterruptedException
	 */
	public Message[] get(int n) throws InterruptedException;
	
	/**
	 * Returns the number of messages currently available in the prodcons buffer
	 * @return int
	 */
	public int nmsg();
	
	/**
	 * Returns the total number of messages that have been put in the buffer
	 * since its creation
	 * @return int 
	 */
	public int totmsg();
}
