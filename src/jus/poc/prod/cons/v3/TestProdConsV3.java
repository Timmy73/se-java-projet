package jus.poc.prod.cons.v3;

import java.io.IOException;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;

import jus.poc.prod.cons.tests.TestProdCons;

public class TestProdConsV3 extends TestProdCons {

	private int	nMaxMessage;
	private int	nMessage	= 5;

	private ProdConsBuffer	buffer;
	private Producer		producers[];
	private Consumer		consumers[];

	public TestProdConsV3() throws InvalidPropertiesFormatException, IOException {
		super(TestProdConsV3.class.getResourceAsStream("config.xml"));

		this.nMaxMessage = Integer.parseInt(conf.getProperty("nMaxMessage"));

		this.buffer		= new ProdConsBuffer(this.bufferSize);
		this.producers	= new Producer[this.nProd];
		this.consumers	= new Consumer[this.nCons];
	}

	/**
	 * Initialisation des threads de consumers et producers.
	 */
	private void init() {
		this.threads.clear(); // On néttoie la liste des threads
		for (int i = 0, j = 0; i < this.nProd || j < this.nCons; i++, j++) { // On créer les différents threads
			if (i < this.nProd) {
				this.producers[i] = new Producer(this.minProd, this.maxProd, this.prodTime, this.buffer);
				this.threads.add(this.producers[i]);
			}
			if (j < this.nCons) {
				this.consumers[j] = new Consumer(this.consTime, this.buffer, this.nMessage);
				this.threads.add(this.consumers[j]);
			}
		}
		Collections.shuffle(this.threads); // Mélange de la liste des threads pour lancement aléatoire
	}

	public void test() throws Exception {
		this.init();
		
		long start = System.currentTimeMillis();
		
		for (int i = 0; i < this.threads.size(); i++)
			this.threads.get(i).start();

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else {
			throw new Exception("Some messages are still in the buffer");
		}
		
		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	public void testBufferSize() throws Exception {
		int bz = this.bufferSize + (int) (Math.random() * (this.bufferMaxSize - this.bufferSize) + 1);
		this.buffer = new ProdConsBuffer(bz);
		this.init();
		
		long start = System.currentTimeMillis();
		
		for (int k = 0; k < this.threads.size(); k++) {
			this.threads.get(k).start();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else
			throw new Exception("Some messsages are still in buffer");
		
		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	/**
	 * Test avec un nombre de message à retirer aléatoire
	 * @throws Exception
	 */
	public void testGetNMessage() throws Exception {
		this.nMessage	= 1 + (int) (Math.random() * (this.nMaxMessage - 1) + 1);
		this.buffer		= new ProdConsBuffer(1);
		this.init();
		
		long start = System.currentTimeMillis();
		
		for (int k = 0; k < this.threads.size(); k++) {
			this.threads.get(k).start();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else
			throw new Exception("Some messsages are still in buffer");
		
		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}

	/**
	 * Test avec un nombre de message à retirer aléatoire et en faisant varier la taille du buffer
	 * @throws Exception
	 */
	public void testGetNMessageBufSize() throws Exception {
		int bz = this.bufferSize + (int) (Math.random() * (this.bufferMaxSize - this.bufferSize) + 1);
	
		this.nMessage	= 1 + (int) (Math.random() * (this.nMaxMessage - 1) + 1);
		this.buffer		= new ProdConsBuffer(bz);
		this.init();
		
		long start = System.currentTimeMillis();
		
		for (int k = 0; k < this.threads.size(); k++) {
			this.threads.get(k).start();
		}

		for (int i = 0; i < this.nProd; i++) {
			this.producers[i].join();
		}

		if (this.buffer.nmsg() <= 0) {
			for (int j = 0; j < this.nCons; j++) {
				this.consumers[j].interrupt();
				this.consumers[j].join();
			}
		} else
			throw new Exception("Some messsages are still in buffer");
		
		long	end		= System.currentTimeMillis();
		float	elapsed	= ((end - start));
		System.out.println("\n\t*** TEST V2 FINISHED ***");
		System.out.println("\n\t - NB MESSAGE TRAITES : " + buffer.totmsg());
		System.out.println("\t - TEMPS ECOULE : " + elapsed + "ms\t" + (elapsed / 1000) + "s");
		System.out.println("\t - Performance : " + (buffer.totmsg() / elapsed) + " message/ms\t"
				+ (buffer.totmsg() / (elapsed / 1000)) + " message/s");
	}
}
