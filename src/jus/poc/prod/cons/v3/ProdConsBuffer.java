package jus.poc.prod.cons.v3;

import jus.poc.prod.cons.message.Message;

public class ProdConsBuffer implements IProdConsBuffer {

	private int	nbCurrMsg;
	private int	nbAddedMsg;
	private int	bufferSize;

	private Message messages[];

	public ProdConsBuffer(int bufferSize) {
		this.bufferSize = bufferSize;

		this.nbAddedMsg	= 0;
		this.nbCurrMsg	= 0;

		this.messages = new Message[this.bufferSize];
	}

	@Override
	synchronized public void put(Message m) throws InterruptedException {

		while (this.nbCurrMsg >= this.messages.length) {
			wait();
		}
		this.messages[this.nbCurrMsg] = m;

		this.nbAddedMsg++;
		this.nbCurrMsg++;
		notifyAll();

	}

	@Override
	synchronized public Message get() throws InterruptedException {
		Message m;

		while (this.nbCurrMsg <= 0) {
			wait();
		}

		m = this.messages[0];
		for (int i = 1; i < this.nbCurrMsg; i++) {
			this.messages[i - 1] = this.messages[i];
		}

		this.nbCurrMsg--;
		this.messages[this.nbCurrMsg] = null;

		notifyAll();

		return m;
	}

	@Override
	public Message[] get(int n) throws InterruptedException {
		
		if(n < 1) throw new IllegalArgumentException("n must greater than 0");
		
		Message[] returns = new Message[n];
		for (int i = 0; i < n; i++) {
			returns[i] = this.get();
		}

		return returns;
	}

	@Override
	public int nmsg() {
		return this.nbCurrMsg;
	}

	@Override
	public int totmsg() {
		return this.nbAddedMsg;
	}

}
