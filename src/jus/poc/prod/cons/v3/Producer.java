package jus.poc.prod.cons.v3;

import jus.poc.prod.cons.message.Message;

public class Producer extends Thread {

	private int		prodTime;
	private int		nbToProduce;
	private int		min;
	private int		max;
	private boolean	finished	= false;

	private ProdConsBuffer buff;

	public Producer(int min, int max, int prodTime, ProdConsBuffer buff) {

		this.min		= min;
		this.max		= max;
		this.prodTime	= prodTime;
		this.buff		= buff;

		this.nbToProduce = this.min + (int) (Math.random() * ((this.max - this.min) + 1));

	}

	@Override
	synchronized public void run() {
		try {
			for (int i = 0; i < this.nbToProduce; i++) {
				Thread.sleep(this.prodTime);
				this.buff.put(new Message("Message " + i + " from producer n° " + this.getId()));
				
				if(TestProdConsV3.DEBUG)
					System.out.println("[PRODUCER] Message generate");
			}
			
			this.finished = true;
			System.out.println("[PRODUCER] Producer "+this.getId()+" finished");
			notify();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isFinished() {
		return finished;
	}

}
