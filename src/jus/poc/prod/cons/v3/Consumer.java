package jus.poc.prod.cons.v3;

import jus.poc.prod.cons.message.Message;

public class Consumer extends Thread {
	private int	consTime;
	private int	nCons;

	private ProdConsBuffer buffer;

	public Consumer(int consTime, ProdConsBuffer buffer) {
		this(consTime, buffer, 5);
	}

	public Consumer(int consTime, ProdConsBuffer buffer, int nMessage) {
		this.consTime	= consTime;
		this.buffer		= buffer;
		this.nCons		= nMessage;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Message[] messages = this.buffer.get(this.nCons);
				sleep(this.consTime);

				if (TestProdConsV3.DEBUG) {
					for (Message m : messages) {
						System.out.println("[CONSUMER] : " + m);
					}
				}
			}
		} catch (InterruptedException e) {
		}
	}
}
