package jus.poc.prod.cons.tests;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

/*
 * Cette classe abstraite permet de factoriser du code.
 */
public abstract class TestProdCons implements ITestProdCons {
	
	/**
	 * Constant permetant d'activer/désactiver l'affichage de message de debug
	 */
	public final static boolean DEBUG = false;

	/**
	 * Attribut permetant la gestion de la taille du buffer
	 */
	protected int	bufferSize, bufferMaxSize;
	
	/**
	 * Respectivement nombre de consomateurs et temps d'une consomation
	 */
	protected int	nCons, consTime;
	
	/**
	 * Respectivement nombre de producteurs et temps d'une production
	 */
	protected int	nProd, prodTime;
	
	/**
	 * Respectivement nombre minimum de producteurs et nombre maximale de producteurs
	 */
	protected int	minProd, maxProd;

	/**
	 * Attribut contenant la configuration.
	 */
	protected Properties	conf;
	
	/**
	 * Liste des consumers et producers confondus
	 */
	protected List<Thread>	threads;

	/**
	 * Permet de charger les propriété contenu dans le fichier de configuration xml
	 * @param file InputSream Fichier de configuration (format xml)
	 * @throws InvalidPropertiesFormatException 
	 * @throws IOException
	 */
	public TestProdCons(InputStream file) throws InvalidPropertiesFormatException, IOException {
		this.conf = new Properties();
		this.conf.loadFromXML(file);

		this.bufferSize		= Integer.parseInt(conf.getProperty("bufSz"));
		this.bufferMaxSize	= Integer.parseInt(conf.getProperty("buffMaxSz"));

		this.nCons		= Integer.parseInt(conf.getProperty("nCons"));
		this.consTime	= Integer.parseInt(conf.getProperty("consTime"));

		this.nProd		= Integer.parseInt(conf.getProperty("nProd"));
		this.prodTime	= Integer.parseInt(conf.getProperty("prodTime"));
		this.minProd	= Integer.parseInt(conf.getProperty("minProd"));
		this.maxProd	= Integer.parseInt(conf.getProperty("maxProd"));
		
		this.threads = new ArrayList<Thread>();
	}
	
	public List<Thread> getThreads(){
		return this.threads;
	}
}
