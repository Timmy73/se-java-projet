package jus.poc.prod.cons.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jus.poc.prod.cons.v4.TestProdConsV4;

public class TestV4 {
	
	private TestProdConsV4 tester;

	@Before
	public void init() throws InvalidPropertiesFormatException, IOException{
		this.tester = new TestProdConsV4();
	}

	@Test
	public void test() {
		try {
			this.tester.test();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testBufferSize() {
		try {
			this.tester.testBufferSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testNProd() {
		try {
			this.tester.testNProd();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testNProdBufSize() {
		try {
			this.tester.testNProdBufSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testFail() {
		try {
			this.tester.testFail();
			fail();
		} catch (IllegalStateException e) {
			assert(true);
		}catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
