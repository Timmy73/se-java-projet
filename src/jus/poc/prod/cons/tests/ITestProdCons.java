package jus.poc.prod.cons.tests;


/**
 * Cette interface peremet de définir le comportement de chaque application de test du ProdConsBuffer
 * @author Raphaël AUDEUR, Thomas FRION
 *
 */
public interface ITestProdCons {
	
	/**
	 * Méthode de test basic, c'ést-à-dire que tous les "paramètres" du test sont fixés à une certaine valeur.
	 * @throws Exception Les exceptions levées sont celles associé au thread de java. 
	 * La méthode léve une exception lorsqu'il reste des messages à consomer lorsque les threads consomateur vont s'arrêter 
	 */
	public void test() throws Exception;
	
	/**
	 * Cette méthode de test fait varier la taille du buffer.
	 * @throws Exception Les exceptions levées sont celles associé au thread de java. 
	 * La méthode léve une exception lorsqu'il reste des messages à consomer lorsque les threads consomateur vont s'arrêter 
	 */
	public void testBufferSize() throws Exception;
}
