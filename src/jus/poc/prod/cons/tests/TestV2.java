package jus.poc.prod.cons.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jus.poc.prod.cons.v2.Consumer;
import jus.poc.prod.cons.v2.Producer;
import jus.poc.prod.cons.v2.TestProdConsV2;

public class TestV2 {

	private TestProdConsV2 tester;

	@Before
	public void init() throws InvalidPropertiesFormatException, IOException {
		this.tester = new TestProdConsV2();
	}

	@Test
	public void test() throws InterruptedException {
		try {
			this.tester.test();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testBufferSize() {
		try {
			this.tester.testBufferSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testNSemaphorEq() {
		try {
			this.tester.testNSemaphorEq();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testNSemaphorNoEq() {

		try {
			this.tester.testNSemaphorNoEq();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testNSemaphorEqBuffSize() {
		try {
			this.tester.testNSemaphorEqBuffSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}

		List<Thread> threads = this.tester.getThreads();

		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

	@Test
	public void testNSemaphorNoEqBuffSize() {
		try {
			this.tester.testNSemaphorNoEqBuffSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

}
