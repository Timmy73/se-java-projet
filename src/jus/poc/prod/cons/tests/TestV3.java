package jus.poc.prod.cons.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jus.poc.prod.cons.v3.TestProdConsV3;

public class TestV3 {
	
	private TestProdConsV3 tester;

	@Before
	public void init() throws InvalidPropertiesFormatException, IOException{
		this.tester = new TestProdConsV3();
	}

	@Test
	public void test() {
		try {
			this.tester.test();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testBufferSize() {
		try {
			this.tester.testBufferSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testGetNMessage() {
		try {
			this.tester.testGetNMessage();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}
	
	@Test
	public void testGetNMessageBufSize() {
		try {
			this.tester.testGetNMessageBufSize();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		List<Thread> threads = this.tester.getThreads();
		for (int i = 0; i < threads.size(); i++) {
			if (threads.get(i).isAlive())
				fail("Some threds are still alive");
		}

		assert (true);
	}

}
